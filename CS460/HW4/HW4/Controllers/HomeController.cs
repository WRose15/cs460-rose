﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HW4.Controllers
{
    public class HomeController : Controller
    {
        /// <summary>
        /// Task 1
        /// Home Index that links to the other three pages of the homework.
        /// </summary>
        /// <returns>A view</returns>
        //GET: Home
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Task 2 - Using GET to send/receive form elements
        /// Example of using the GET form submit method. Jumbles the two user
        /// typed sentences.
        /// </summary>
        /// <returns>A random string randomly composed of two sentences.</returns>
        public ActionResult Page1()
        {
            if (Request.QueryString["submit"] == "Submit")
            {
                MashSentences(Request.QueryString["firstSentence"], Request.QueryString["secondSentence"]);
            }

            return View();
        }

        /// <summary>
        /// Task 3 - Use POST
        /// Uses GET to retrieve the form and POST to send the filled out form
        /// </summary>
        /// <returns>Says hi to the user and uses the name entered</returns>
        [HttpGet]
        public ActionResult Page2()
        {
            return View();
        }

        /// <summary>
        /// Task 3 - POST page
        /// </summary>
        /// <param name="form">Filled out with First and Last name</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Page2(FormCollection form)
        {
            ViewBag.message = "Hello there " + form["firstName"] + " " + form["lastName"];
            return View();
        }

        /// <summary>
        /// Task 3 - Loan Calculator
        /// </summary>
        /// <returns>Page with the form</returns>
        [HttpGet]
        public ActionResult Page3()
        {
            ViewBag.amount = 12000;
            ViewBag.interest = 3.5;
            ViewBag.downPayment = 0;
            ViewBag.months = 60;
            ViewBag.final = false;
            return View();
        }

        /// <summary>
        /// POST for car loan calculator. Will calculate the original down
        /// payment and also calculate it at 10% and 20% downpayments.
        /// </summary>
        /// <param name="amount"> Price of vehicle without cents</param>
        /// <param name="interest">Rate as a double. Need to divide by 100 for percentage</param>
        /// <param name="months">Years of loan as months</param>
        /// <param name="downPayment">Cash up front to lower cost of loan.</param>
        /// <returns>Calculated monthly payments</returns>
        [HttpPost]
        public ActionResult Page3(int amount, double interest, int months, int downPayment)
        {
            // Behavior is tied to if (IsPost && flag) statement in html. ViewBag.final acts
            // as the flag to display table or input form.
            if (amount - downPayment <= 0)
            {
                ViewBag.amount = amount;
                ViewBag.interest = interest;
                ViewBag.downPayment = downPayment;
                ViewBag.months = months;
                ViewBag.final = false;
                ViewBag.message = "The down payment is greater than the loan amount.";
                return View();
            }

            // Rounding is fine here. Explicitly declared since var is used
            // in two places.
            int downPayment2 = (int)(amount * 0.10);
            int downPayment3 = (int)(amount * 0.20);

            ViewBag.amount = amount.ToString("C");
            ViewBag.interest = (interest / 100).ToString("P");
            ViewBag.months = months;
            ViewBag.downPayment = downPayment.ToString("C");
            ViewBag.downPayment2 = downPayment2.ToString("C");
            ViewBag.downPayment3 = downPayment3.ToString("C"); 
            decimal monthlyPay = (decimal)CalculateMonthly(amount - downPayment, months, interest);
            decimal monthlyPay2 = (decimal)CalculateMonthly(amount - downPayment2, months, interest);
            decimal monthlyPay3 = (decimal)CalculateMonthly(amount - downPayment3, months, interest);
            ViewBag.monthly = monthlyPay.ToString("C");
            ViewBag.monthly2 = monthlyPay2.ToString("C");
            ViewBag.monthly3 = monthlyPay3.ToString("C");
            ViewBag.total = (monthlyPay * months).ToString("C");
            ViewBag.total2 = (monthlyPay2 * months).ToString("C");
            ViewBag.total3 = (monthlyPay3 * months).ToString("C");
            ViewBag.final = true;
            return View();
        }
        /// <summary>
        /// Breaks the string into an array of substrings based on spaces. It
        /// then randomly mixes them together to form a new string which is
        /// then placed in a ViewBag for the webpage to display.
        /// </summary>
        /// <param name="first">string A sentence with spaces</param>
        /// <param name="second">string A sentence with spaces</param>
        private void MashSentences(string first, string second)
        {
            Random rnd = new Random();
            Random rnd2 = new Random();
            string[] firstArray = first.Split(new char[0], StringSplitOptions.RemoveEmptyEntries).OrderBy(x => rnd.Next()).ToArray();
            string[] secondArray = second.Split(new char[0], StringSplitOptions.RemoveEmptyEntries).OrderBy(x => rnd2.Next()).ToArray();

            string combined = "";
            // Find the longer string to prevent index error
            if (firstArray.Length > secondArray.Length)
            {
                int i = 0;
                for (i = 0; i < secondArray.Length; i++)
                {
                    combined += firstArray[i] + " " + secondArray[i] + " ";
                }
                for (;i < firstArray.Length;i++)
                {
                    combined += firstArray[i] + " ";
                }
            }
            else
            {
                int i = 0;
                for (i = 0; i < firstArray.Length; i++)
                {
                    combined += secondArray[i] + " " + firstArray[i] + " ";
                }
                for (; i < secondArray.Length; i++)
                {
                    combined += secondArray[i] + " ";
                }
            }
            ViewBag.first = combined;
            ViewBag.second = "There are " + (firstArray.Length + secondArray.Length) + " words in both sentences.";
        }

        private double CalculateMonthly (int amount, int months, double interest)
        {
            double monthlyInterest = (interest / 100) / 12;
            return ((monthlyInterest) * amount) / (1 - (Math.Pow((1 + monthlyInterest), -(months))));
        }
    }
}