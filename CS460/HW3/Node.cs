﻿using System;

/// <summary>A simple singly linked node class. This implementation comes from
/// a translation of a java file.</summary>
public class Node
{
    // payload
    public object Data { get; set; }

    // Reference to the next Node in the chain
    public Node Next { get; set; }

    // Default constructor with no arguments.
    public Node() { }

    // Used to replicate java code at point where it requires a constructor
    // using 2 arguments.
    public Node(Object data, Node next)
    {
        this.Data = data;
        this.Next = next;
    }
}
