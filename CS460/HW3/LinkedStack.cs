﻿using System;
/// <summary>A singly linked stack implementation in C#</summary>
/// 
public class LinkedStack : StackADT
{
    private Node top;

    /// <summary>
    /// Initialize a LinkStack
    /// </summary>
	public LinkedStack()
	{
        top = null; // Empty stack condition
	}

    /// <summary>
    /// Creates a node with the item in it and places it on top of the stack.
    /// </summary>
    /// <param name="newItem">Item to put on top</param>
    /// <returns>Return a reference to the top of the stack</returns>
    public Object push(Object newItem)
    {
        if( newItem == null)
        {
            return null;
        }
        // top is current top of LinkedStack
        Node newNode = new Node { Data = newItem, Next = top };
        // top is new top node of LinkedStack
        top = newNode;
        return newItem;
    }

    /// <summary>
    /// Returns and removes the item on top of the stack
    /// </summary>
    /// <returns>The value of the item on top of the stack</returns>
    public Object pop()
    {
        if(isEmpty() )
        {
            return null;
        }
        Object topItem = top.Data;
        top = top.Next;
        return topItem;
    }

    /// <summary>
    /// Looks at the value of the top of the stack without removing it.
    /// </summary>
    /// <returns>The value of the item on the top of the stack.</returns>
    public Object peek()
    {
        if(isEmpty())
        {
            return null;
        }
        return top.Data;
    }

    /// <summary>
    /// Checks the stack to see if it is empty
    /// </summary>
    /// <returns>Bool relating to emptiness of stack</returns>
    public Boolean isEmpty()
    {
        return top == null;
    }

    /// <summary>
    /// Removes reference to the first node of the stack effectively clearing it.
    /// </summary>
    public void clear()
    {
        top = null;
    }
}
