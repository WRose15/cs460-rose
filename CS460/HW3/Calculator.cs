﻿using System;


namespace HW3
{
    /// <summary>
    /// A translation of a Java program that accepts Postfix strings from the
    /// command prompt and evaluates them.
    /// </summary>
    class Calculator
    {
        // stack used to hold the operands for postfix.
        StackADT calStack = new LinkedStack();

        /// <summary>
        /// Entry point method
        /// </summary>
        /// <param name="args">Command line arguments that are ignored</param>
        static void Main(string[] args)
        {
            Boolean loopRunning = true;
            Calculator app = new Calculator();
            Console.WriteLine("\nPostfix Calculator. Recognizes these operators: + - * /");
            Console.WriteLine("Example: 4 + 4 is 4 4 + in postfix notation.");

            while (loopRunning)
            {
                try
                {
                    loopRunning = app.doCalculation();
                }
                catch (ArgumentException ex)
                {
                    Console.WriteLine(ex);
                }
            }
        }

        /// <summary>
        /// Gets input string from user, splits it into an array of strings
        /// and then passes it off for it to be calculated.
        /// </summary>
        /// <returns>true if calculation was successful and false if user wishes
        /// to quit</returns>
        /// <exception cref="ArgumentException">Thrown when a bad value is encountered.</exception>
        private Boolean doCalculation()
        {
            Console.WriteLine("Type quit to exit.");

            // raw string from command line.
            string input = Console.ReadLine();

            // Will only quit if the first thing on the line is 'quit' (no whitespace)
            if (input == "quit" || input == "Quit")
            {
                Console.WriteLine("Goodbye Dave");
                return false;
            }
            else
            {
                // char[0] (whitespace) is the delimiting character. Removes empty/null strings
                string[] tokenInput = input.Split(new char[0], StringSplitOptions.RemoveEmptyEntries);
                if (input == null || input.Length == 0)
                {
                    throw new System.ArgumentException("Null or the empty string are not valid postfix expressions.");
                }
                calStack.clear();

                try
                {
                    Console.WriteLine(evaluatePostScript(tokenInput));
                }
                catch (ArgumentException e)
                {
                    Console.WriteLine(e);
                }
            }
            return true;
        }

        /// <summary>
        /// Evaluates the postfix expression using a switch.
        /// </summary>
        /// <param name="input">Postfix expression as an array of strings</param>
        /// <returns>The answer as a double</returns>
        /// <exception cref="ArgumentException">Thrown when a bad value is encountered.</exception>
        private double evaluatePostScript(string[] input)
        {
            double a;
            double b;
            double c;

            foreach (string s in input)
            {
                switch (s)
                {
                    case "+":
                    case "-":
                    case "/":
                    case "*":
                        if (calStack.isEmpty())
                        {
                            throw new System.ArgumentException("Improper format. Stack became empty when expecting second operand.");
                        }
                        b = (double)calStack.pop();
                        if (calStack.isEmpty())
                        {
                            throw new System.ArgumentException("Improper format. Stack became empty when expecting second operand.");
                        }
                        a = (double)calStack.pop();
                        c = doOperation(a, b, s);
                        try
                        {
                            calStack.push(c);
                        }
                        catch (ArgumentException e)
                        {
                            throw e;
                        }
                        break;
                    
                    // For the quit/Quit that is not at the beginning of the string.
                    case "quit":
                        throw new System.ArgumentException("Please type quit without anything before it.");
                    case "Quit":
                        throw new System.ArgumentException("Please type Quit without anything before it.");
                    default:
                        double n;
                        if (double.TryParse(s, out n))
                            calStack.push(n);
                        else
                        throw new System.ArgumentException("I'm a computer, not a psychic! What's this parameter?", s);
                        break;
                }
            }
            return (double)calStack.pop();
        }

        /// <summary>
        /// Method to perform the arithmetic on the 2 operands and the operator.
        /// Split from the evaluatePostScript method to make the code cleaner.
        /// </summary>
        /// <param name="a">First operand</param>
        /// <param name="b">Second operand</param>
        /// <param name="s">Operator</param>
        /// <returns>The answer</returns>
        /// <exception cref="ArgumentException">Thrown when a bad value is encountered.</exception>
        private double doOperation(double a, double b, string s)
        {
            // variable for the answer to the current operation
            double c = 0.0;

            switch (s)
            {
                case "+":
                    c = a + b;
                    break;
                case "-":
                    c = a - b;
                    break;
                case "/":
                    if (b != 0)
                    {
                        c = a / b;
                    }
                    else
                    {
                        // 0 would be a bad arguement here.
                        throw new System.ArgumentException("Stop trying to divide by 0");
                    }
                    break;
                case "*":
                    c = a * b;
                    break;
                default:
                    // for anything else that shouldn't occur.
                    throw new System.ArgumentException("Something went wrong Jim");
            }
            return c;
        }

    }
}
