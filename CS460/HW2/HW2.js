  /* Homework 2
                                                                                                                                                            A matrix calculator for the web based on a previous version made with Javascript
                                                                                                                                                        */

  /* Global variables for matrix dimensions. Matrix A = m x k, Matrix B = k x n */
  var matAm = 0;
  var matAk = 0;
  var matBk = 0;
  var matBn = 0;

  /* Check the inner dimensions for compatiblity */
  function checkMatrix() {
      $("#mismatch").empty();
      $("#results").empty();
      if (matAm < 1 || matAk < 1 || matBk < 1 || matBn < 1) {
          $("#mismatch").html("Please enter a number greater than 0 in all fields.");
          console.log("Matrix A: " + matAm + " X " + matAk + "\n" + "Matrix B: " + matBk + " X " + matBn);
          return false;
      }
      if (matAk == matBk) {
          return true;
      }
      $("#mismatch").html("The dimensions do not match.");
      return false;
  }

  /* Setup the input grid for the matrices*/
  function drawMatrices() {
      var divContainer = $("<div>", {
          "class": "container"
      });
      var divRow = $("<div>", {
          "class": "row"
      });
      divContainer.append(divRow);

      /* Outer loop that adds Matrix A and B to the container */
      for (var or = 0; or < 2; or++) {
          var divCol = $("<div>", {
              "class": "col-md-6"
          });
          var m = 'A';
          if (or == 1) {
              m = 'B';
          }
          var fieldSet = $("<fieldSet>", {
              "id": "matField" + m
          });
          var legen = $("<span>", {
              "align": "center"
          }).html("Matrix " + m + ":");

          legen.appendTo(fieldSet);
          fieldSet.appendTo(divCol);
          var tab = $("<table>", {
              "align": "center"
          });
          var tbod = $("<tbody>");

          var maxRow = 0;
          var maxCol = 0;
          if (or == 0) {
              maxRow = matAm;
              maxCol = matAk;
          } else {
              maxRow = matBk;
              maxCol = matBn;
          }
          for (var ml = 0; ml < maxRow; ml++) {
              var row = $("<tr>");
              for (var il = 0; il < maxCol; il++) {
                  var cell = $("<td>");
                  cell.append($("<input>", {
                      "type": "number",
                      "class": "form-control",
                      "placeholder": "00",
                      "maxlength": "2",
                      "required": "",
                      id: "" + or + ml + il
                  }));
                  cell.appendTo(row);
              }
              row.appendTo(tbod);
          }
          tbod.appendTo(tab);
          tab.appendTo(fieldSet);
          divCol.appendTo(divRow);
      }

      $("#matrices").empty();
      $("#matrices").append(divContainer);
      $("#matrices").append($("<br>"));
      $("#matrices").append($("<center>", {
          "id": "calculateButton"
      }));
      $("#calculateButton").append($("<input>", {
          "id": "calculate",
          "type": "submit",
          "value": "Calculate",
          "onclick": "calculate()"
      }));
      $("#calculateButton").append($("<br>"));
      $("#calculateButton").append($("<span>", {
          "id": "badCell",
          "class": "error"
      }));
  }

  $("#step1").click(function(event) {
      event.preventDefault();
      matAm = $("#matrixAm").val();
      matAk = $("#matrixAk").val();
      matBk = $("#matrixBk").val();
      matBn = $("#matrixBn").val();
      var mobj = checkMatrix();

      $("#matrices").empty();
      if (mobj) {
          console.log("The inner dimensions match");
          drawMatrices();
      }
  });

  function calculate() {
      // Setup an array of arrays for MatrixA.
      $("#results").empty();
      $("#badCell").empty();
      var matrixA = [];
      for (var i = 0; i < matAm; i++) {
          matrixA[i] = [];
          for (var j = 0; j < matAk; j++) {
              var cellIndex = "#0" + i + j;
              var tempValue = $(cellIndex).val();
              if (tempValue == "") {
                  $("#badCell").html("Bad value at " + "Matrix A - Row " + (i + 1) + " Column " + (j + 1));
                  return;
              }
              matrixA[i][j] = tempValue;
          }
      }

      // Setup an array of arrays for MatrixB, this will be transposed.
      var matrixB = [];
      for (var ii = 0; ii < matBn; ii++) {
          matrixB[ii] = [];
          for (var jj = 0; jj < matBk; jj++) {
              var cellIndex2 = "#1" + jj + ii;
              var tempValue = $(cellIndex2).val();
              if (tempValue == "") {
                  $("#badCell").html("Bad value at " + "Matrix B - Row " + (jj + 1) + " Column " + (ii + 1));
                  return;
              }
              matrixB[ii][jj] = tempValue;
          }
      }

      // Calculate the result matrix.
      var matrixC = [];
      for (ci = 0; ci < matAm; ci++) {
          matrixC[ci] = [];
          for (cj = 0; cj < matBn; cj++) {
              matrixC[ci][cj] = multiply(matrixA[ci], matrixB[cj]);
          }
      }

      drawResultTable(matrixC);
  };

  function multiply(matA, matB) {
      var temp = 0;
      for (var t = 0; t < matBk; t++) {
          temp += matA[t] * matB[t];
      }
      return temp;
  };

  function drawResultTable(matrixC) {
      var divContainer = $("<div>", {
          "class": "container"
      });
      var divRow = $("<div>", {
          "class": "row"
      });
      divContainer.append(divRow);

      var divCol = $("<center>");
      var fieldSet = $("<fieldSet>", {
          "id": "matFieldC"
      });
      var legen = $("<span>", {
          "align": "center"
      }).html("Matrix C ");

      legen.appendTo(fieldSet);
      fieldSet.appendTo(divCol);

      var tab = $("<table>", {
          "align": "center",
          "id": "resultTable"
      });
      var tbod = $("<tbody>");

      for (var row = 0; row < matAm; row++) {
          var trow = $("<tr>");
          for (var col = 0; col < matBn; col++) {
              var cell = $("<td>");
              cell.append(matrixC[row][col]);
              cell.appendTo(trow);
          }
          trow.appendTo(tbod);
      }

      tbod.appendTo(tab);
      tab.appendTo(fieldSet);
      divCol.appendTo(divRow);

      $("#results").empty();
      $("#results").append($("<br>"));
      $("#results").append(divContainer);
  }
