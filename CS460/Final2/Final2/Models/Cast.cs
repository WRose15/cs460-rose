namespace Final2.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Cast")]
    public partial class Cast
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(100)]
        public string Actor { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(50)]
        public string Movie { get; set; }

        public virtual Movy Movy { get; set; }
    }
}
