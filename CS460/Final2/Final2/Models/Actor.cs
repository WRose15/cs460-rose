namespace Final2.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Actor
    {
        [Key]
        [Column("Actor")]
        [StringLength(100)]
        public string Actor1 { get; set; }
    }
}
