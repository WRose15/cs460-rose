﻿using Final2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Final2.Controllers
{
    public class HomeController : Controller
    {
        private MovieContext db = new MovieContext();

        public ActionResult Index()
        {
            var actors = db.Actors.ToList();
            return View(actors);
        }

        public ActionResult Actors()
        {
            return View(db.Actors.ToList());
        }

        public ActionResult Cast()
        {
            return View(db.Casts.ToList());
        }

        public JsonResult movieList(string id)
        {
            var movies = db.Casts.Where(x => x.Actor == id).Select(cmd => new
            {
                Name = cmd.Actor,
                Movie = cmd.Movie,
                Direct = cmd.Movy.Director
            }).ToList();
            return Json(movies, JsonRequestBehavior.AllowGet);
        }
    }
}