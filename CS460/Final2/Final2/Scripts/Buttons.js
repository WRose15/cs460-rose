﻿$("#one").click(function (event) {
    getMovies("Felicity Jones");
})
$("#two").click(function () {
    getMovies("Mads Mikkelsen");
})
$("#three").click(function () {
    getMovies("Benedict Cumberbatch");
})
$("#four").click(function () {
    getMovies("Rachel McAdams");
})

function getMovies(name) {
    var src = "/HOME/movieList/" + name;
    console.log(src);
    $.ajax({
        type: "GET",
        url: src,
        success: displayMovies
    })
}

function displayMovies(data) {
    $("#listHere").empty();
    var table = "<table><tr><th>Actor</th><th>Movie</th><th>Director</th></tr>";
    for (var i = 0; i < data.length; i++) {
        table += "<tr><td>" + data[i].Name + "</td><td>" + data[i].Movie + "</td><td>" + data[i].Direct + "</td></tr>";
    }
    table += "</table>";
    $("#listHere").append(table);
}