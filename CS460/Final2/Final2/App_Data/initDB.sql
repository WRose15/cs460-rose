﻿
-- ########### Movies ###########
CREATE TABLE [dbo].[Movies]
(
	[Title] NVARCHAR (50) UNIQUE NOT NULL,
	[Year] INT NOT NULL,
	[Director] NVARCHAR (50) NOT NULL,
	CONSTRAINT [PK_dbo.Movies] PRIMARY KEY CLUSTERED ([Title] ASC)
);

-- ########### Actors ###########
CREATE TABLE [dbo].[Actors]
(
	[Actor] NVARCHAR (100) UNIQUE NOT NULL,
	CONSTRAINT [PK_dbo.ArtWorks] PRIMARY KEY CLUSTERED ([Actor] ASC),

);

-- ########### Directors ###########
CREATE TABLE [dbo].[Directors]
(
	[Name] NVARCHAR (100) UNIQUE NOT NULL,
	CONSTRAINT [PK_dbo.Name] PRIMARY KEY CLUSTERED ([Name] ASC),
);

-- ########### Cast ###
CREATE TABLE [dbo].[Cast]
(
	[Actor] NVARCHAR (100) NOT NULL,
	[Movie] NVARCHAR (50) NOT NULL,
	CONSTRAINT [PK_dbo.Actor] PRIMARY KEY CLUSTERED ([Actor],[Movie] ASC),
	CONSTRAINT [FK_dbo.Cast_dbo.Movies] FOREIGN KEY ([Movie]) REFERENCES [dbo].[Movies] ([Title])
);
