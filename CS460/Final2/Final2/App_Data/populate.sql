﻿-- ########### Movies #########
INSERT INTO [dbo].[Movies](Title, Year, Director)VALUES('Rogue One: A Star Wars Story', 2016, 'Gareth Edwards');
INSERT INTO [dbo].[Movies](Title, Year, Director)VALUES('Doctor Strange', 2016, 'Scott Derrickson');
INSERT INTO [dbo].[Movies](Title, Year, Director)VALUES('The Imitation Game', 2014, 'Morten Tyldum');
INSERT INTO [dbo].[Movies](Title, Year, Director)VALUES('The Theory of Everything', 2014, 'James Marsh');

-- ########### Actors ###########
INSERT INTO [dbo].[Actors](Actor)VALUES('Felicity Jones');
INSERT INTO [dbo].[Actors](Actor)VALUES('Mads Mikkelsen');
INSERT INTO [dbo].[Actors](Actor)VALUES('Benedict Cumberbatch');
INSERT INTO [dbo].[Actors](Actor)VALUES('Rachel McAdams');

-- ########### Directors ###########
INSERT INTO [dbo].[Directors](Name)VALUES('Gareth Edwards');
INSERT INTO [dbo].[Directors](Name)VALUES('Scott Derrickson');
INSERT INTO [dbo].[Directors](Name)VALUES('Morten Tyldum');
INSERT INTO [dbo].[Directors](Name)VALUES('James Marsh');

-- ########### Cast ################
INSERT INTO [dbo].[Cast](Actor, Movie)VALUES('Felicity Jones', 'Rogue One: A Star Wars Story');
INSERT INTO [dbo].[Cast](Actor, Movie)VALUES('Mads Mikkelsen', 'Rogue One: A Star Wars Story');
INSERT INTO [dbo].[Cast](Actor, Movie)VALUES('Benedict Cumberbatch', 'Doctor Strange');
INSERT INTO [dbo].[Cast](Actor, Movie)VALUES('Rachel McAdams', 'Doctor Strange');
INSERT INTO [dbo].[Cast](Actor, Movie)VALUES('Benedict Cumberbatch', 'The Imitation Game');
INSERT INTO [dbo].[Cast](Actor, Movie)VALUES('Felicity Jones', 'The Theory of Everything');
INSERT INTO [dbo].[Cast](Actor, Movie)VALUES('Mads Mikkelsen', 'Doctor Strange');