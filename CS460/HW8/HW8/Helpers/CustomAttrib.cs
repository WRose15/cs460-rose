﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HW8.Helpers
{
    public class PastDateAttribute : System.ComponentModel.DataAnnotations.ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            return value != null && (DateTime) value < DateTime.Now;
        }
    }
}