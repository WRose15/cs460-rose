﻿using HW8.Models;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace HW8.Controllers
{
    public class PirateController : Controller
    {
        private PirateContext db = new PirateContext();
        public int PageSize = 3;

        // GET: Pirate
        public ActionResult Pirates(int page = 1)
        {
            PirateListViewModel model = new PirateListViewModel
            {
                Pirates = db.Pirates.OrderByDescending(p => p.PirateName).Skip((page - 1) * PageSize).Take(PageSize),
                PagingInfo = new PagingInfo
                {
                    CurrentPage = page,
                    ItemsPerPage = PageSize,
                    TotalItems = db.Pirates.Count()
                }
            };
            return View(model);
        }

        // GET: Pirate/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var pirate = db.Pirates.Find(id);
            if (pirate == null)
            {
                return HttpNotFound();
            }
           
            return View(pirate);
        }

        // GET: Pirate/Create
        public ActionResult Create()
        {
            Pirate rookie = new Pirate();
            rookie.ShanghaiDate = DateTime.Now;
            return View(rookie);
        }

        // POST: Pirate/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "PirateID,PirateName,ShanghaiDate,Crews")] Pirate pirateInfo)
        {
            if (ModelState.IsValid)
            {
                db.Entry(pirateInfo).State = EntityState.Added;
                db.SaveChanges();
                return RedirectToAction("Pirates");
            }
            return View(pirateInfo);
        }

        // GET: Pirate/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var pirate = db.Pirates.Find(id);
            if (pirate == null)
            {
                return HttpNotFound();
            }
            return View(pirate);
        }

        // POST: Pirate/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include ="PirateID,PirateName,ShanghaiDate,Crews")] Pirate pirateInfo)
        {
            if (ModelState.IsValid)
            {
                db.Entry(pirateInfo).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Pirates");
            }
            return View(pirateInfo);
        }

        // GET: Pirate/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var pirate = db.Pirates.Find(id);
            if (pirate == null)
            {
                return HttpNotFound();
            }
            return View(pirate);
        }

        // POST: Pirate/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id)
        {
            Pirate pirate = db.Pirates.Find(id);
            db.Pirates.Remove(pirate);
            db.SaveChanges();
            return RedirectToAction("Pirates");
        }
    }
}
