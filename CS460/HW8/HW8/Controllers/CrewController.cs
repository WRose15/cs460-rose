﻿using HW8.Models;
using System.Linq;
using System.Web.Mvc;

namespace HW8.Controllers
{
    public class CrewController : Controller
    {
        private PirateContext db = new PirateContext();

        // GET: Booty
        public ActionResult Booty()
        {
            return View();
        }

        // GET: Crew
        public ActionResult Crews()
        {
            var deckHands = db.Crews.OrderBy(p => p.ShipID).ToList();
            return View(deckHands);
        }
    }
}
