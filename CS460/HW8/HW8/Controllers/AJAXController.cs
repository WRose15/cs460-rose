﻿using HW8.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HW8.Controllers
{
    public class AJAXController : Controller
    {
        //AJAX - GET
        /// <summary>
        /// For the AJAX call that retrieves a list of booty
        /// </summary>
        /// <returns> PirateName/Booty sum list.</returns>
        public JsonResult BootyTotals()
        {
            PirateContext db = new PirateContext();
            var totals = db.Crews.GroupBy(c => c.Pirate.PirateName).Select(p => new
            {
                name = p.FirstOrDefault().Pirate.PirateName,
                Booty = p.Select(b => b.BootyValue).Sum()
            }).OrderByDescending(cb => cb.Booty).ToList();

            return Json(totals, JsonRequestBehavior.AllowGet);
        }
    }
}