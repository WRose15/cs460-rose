﻿using HW8.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HW8.Controllers
{
    public class ShipController : Controller
    {
        private PirateContext db = new PirateContext();

        // GET: Ship
        public ActionResult Ships()
        {
            var crewBuckets = db.Ships.OrderBy(s => s.Tonnage);
            return View(crewBuckets);
        }
    }
}
