﻿// View Model for paging as found in : PRO ASP.NET MVC 5, BY ADAM FREEMAN
using System.Collections.Generic;

namespace HW8.Models
{
    public class PirateListViewModel
    {
        public IEnumerable<Pirate> Pirates { get; set; }
        public PagingInfo PagingInfo { get; set; }

    }
}