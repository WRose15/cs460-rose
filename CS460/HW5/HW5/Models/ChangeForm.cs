﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HW5.Models
{
    public class ChangeForm
    {
        public int ID { get; set; }

        [Display(Name = "Last Name"), Required]
        public string LastName { get; set; }

        [Display(Name = "First Name"), Required]
        public string FirstName { get; set; }

        [Display(Name = "Middle Initial"), Required]
        public string MiddleInit { get; set; }

        [Display(Name = "Today's Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}"), Required]
        public DateTime Date { get; set; }

        [Display(Name = "Phone Number")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:(###) ###-####}"), Required]
        [Range(1000000000, 9999999999, ErrorMessage = "Please use 10 digits")]
        public long PhoneNumber { get; set; }

        [Display(Name = "Catalog Year")]
        [Required]
        [Range(2010, 2020, ErrorMessage = "Must be between 2010 and 2020")]
        public int CatalogYear { get; set; }

        [Display(Name = "Student ID#"), Required]
        [StringLength(9)]
        public string StudentID { get; set; }

        [Display(Name = "WOU Email Address"), Required]
        public string WOUEmail { get; set; }

        [Display(Name = "Change Major"), Required]
        public bool ChangeOfMajor { get; set; }

        [Display(Name = "Change Minor"), Required]
        public bool ChangeOfMinor { get; set; }

        [Display(Name = "Change Advisor"), Required]
        public bool ChangeOfAdvisor { get; set; }

        [Display(Name = "Major")]
        public string Major1 { get; set; }

        [Display(Name = "2nd Major")]
        public string Major2 { get; set; }

        [Display(Name = "Minor")]
        public string Minor1 { get; set; }

        [Display(Name = "2nd Minor")]
        public string Minor2 { get; set; }

        [Display(Name = "Advisor")]
        public string Advisor { get; set; }
    }
}