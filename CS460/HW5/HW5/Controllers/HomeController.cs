﻿using HW5.DAL;
using HW5.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HW5.Controllers
{
    public class HomeController : Controller
    {
        private ChangeFormContext db = new ChangeFormContext();

        /// <summary>
        /// Landing page for the form.
        /// </summary>
        /// <returns>Html page</returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        ///  GET: Page with form to create change request
        /// </summary>
        /// <returns>Return HTML page and ChangeForm object with Data and 
        /// year filled out.
        /// </returns>
        [HttpGet]
        public ActionResult Change()
        {
            ChangeForm init = new ChangeForm();
            init.Date = DateTime.Today;
            init.CatalogYear = DateTime.Today.Year;
            return View(init);
        }

        /// <summary>
        /// POST: This will enter the request form into the database and then
        /// redirect back to the index if successful. 
        /// </summary>
        /// <param name="changeRequest"></param>
        /// <returns>Returns to index on success or returns form with errors
        /// marked</returns>
        [HttpPost]
        public ActionResult Change(ChangeForm changeRequest)
        {
            if(ModelState.IsValid)
            {
                db.ChangeForms.Add(changeRequest);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(changeRequest);
        }

        /// <summary>
        /// Lists the Change Forms that are currently in the database.
        /// </summary>
        /// <returns>A list of forms along with the html page to display them.</returns>
        public ActionResult ChangeList()
        {
            return View(db.ChangeForms.ToList());
        }
    }
}