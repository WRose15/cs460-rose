-- Create tables and populate with seed data
--    follow naming convention: "Users" table contains rows that are each "User" objects
-- ***********  Attach ***********
CREATE DATABASE [ChangeForms] ON
PRIMARY (NAME=[ChangeForms], FILENAME='$(dbdir)\ChangeForms.mdf')
LOG ON (NAME=[ChangeForms_log], FILENAME='$(dbdir)\ChangeForms_log.ldf');
--FOR ATTACH;
GO
USE [ChangeForms];
GO
-- *********** Drop Tables ***********
IF OBJECT_ID('dbo.ChangeForms','U') IS NOT NULL
	DROP TABLE [dbo].[ChangeForms];
GO
-- ########### ChangeForms ###########
CREATE TABLE [dbo].[ChangeForms]
(
	[ID] INT IDENTITY (1,1) NOT NULL,
	[LastName] NVARCHAR (50) NOT NULL,
	[FirstName] NVARCHAR (50) NOT NULL,
	[MiddleInit] NVARCHAR (1) NOT NULL,
	[Date] DATETIME NOT NULL,
	[PhoneNumber] BIGINT NOT NULL,
	[CatalogYear] INT NOT NULL,
	[StudentID] CHAR (9) NOT NULL,
	[WOUEmail] NVARCHAR (50) NOT NULL,
	[ChangeOfMajor] BIT NOT NULL,
	[ChangeOfMinor] BIT NOT NULL,
	[ChangeOfAdvisor] BIT NOT NULL,
	[Major1] NVARCHAR (25),
	[Major2] NVARCHAR (25),
	[Minor1] NVARCHAR (25),
	[Minor2] NVARCHAR (25),
	[Advisor] NVARCHAR (50),
	CONSTRAINT [PK_dbo.ChangeForms] PRIMARY KEY CLUSTERED ([ID] ASC)
);

BULK INSERT [dbo].[ChangeForms]
	FROM '$(dbdir)\SeedData\ChangeForms.csv'		-- ID,FirstName,LastName,DOB
	WITH
	(
		FIELDTERMINATOR = ',',
		ROWTERMINATOR	= '\n',
		FIRSTROW = 2
	);
GO

-- ***********  Detach ***********
USE master;
GO

ALTER DATABASE [ChangeForms] SET SINGLE_USER WITH ROLLBACK IMMEDIATE
GO

EXEC sp_detach_db 'ChangeForms', 'true'
