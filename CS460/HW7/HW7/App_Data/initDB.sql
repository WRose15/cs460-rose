-- Create tables and populate with seed data
--    follow naming convention: "Users" table contains rows that are each "User" objects
-- ***********  Attach ***********
CREATE DATABASE [WebLog] ON
PRIMARY (NAME=[WebLog], FILENAME='$(dbdir)\WebLog.mdf')
LOG ON (NAME=[WebLog_log], FILENAME='$(dbdir)\WebLog_log.ldf');
--FOR ATTACH;
GO
USE [WebLog];
GO
-- *********** Drop Tables ***********
IF OBJECT_ID('dbo.WebLog','U') IS NOT NULL
	DROP TABLE [dbo].[WebLog];
GO
-- ########### WebLog ###########
CREATE TABLE [dbo].[WebLog]
(
	[ID] INT IDENTITY (1,1) NOT NULL,
	[TimeStamp] DATETIME NOT NULL,
	[IPAddress] varchar(45) NOT NULL,
	[Browser] NVARCHAR (20) NOT NULL,
	[Request] NVARCHAR (100) NOT NULL,
	CONSTRAINT [PK_dbo.WebLog] PRIMARY KEY CLUSTERED ([ID] ASC)
);

--BULK INSERT [dbo].[WebLog]
--	FROM '$(dbdir)\SeedData\WebLog.csv'		-- ID,FirstName,LastName,DOB
--	WITH
--	(
--		FIELDTERMINATOR = ',',
--		ROWTERMINATOR	= '\n',
--		FIRSTROW = 2
--	);
--GO

-- ***********  Detach ***********
USE master;
GO

ALTER DATABASE [WebLog] SET SINGLE_USER WITH ROLLBACK IMMEDIATE
GO

EXEC sp_detach_db 'WebLog', 'true'
