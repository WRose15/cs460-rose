﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using static HW7.Models.Helpers;

namespace HW7.Controllers
{
    public class FlotDataController : Controller
    {

        /// <summary>
        /// Method to return the closing price of the desired stock. 
        /// </summary>
        /// <param name="stock">Stock Symbol</param>
        /// <param name="start">Date to start at given as MM/dd/yyyy</param>
        /// <param name="end">Date to end at given as MM/dd/yyyy</param>
        /// <returns></returns>
        public JsonResult GetFile(string stock, DateTime start, DateTime end)
        {
            stock = stock.ToUpper();

            // Start of log to log request sent
            WebLog entry = new WebLog();
            DAL.WebLogContext db = new DAL.WebLogContext();

            entry.TimeStamp = DateTime.Now;
            entry.IPAddress = Request.UserHostAddress;
            entry.Browser = Request.Browser.Browser;
            entry.Request = Request.QueryString.ToString();
            db.WebLogs.Add(entry);
            db.SaveChanges();
            // end of database access

            var startMonth = start.Month - 1;
            var startDay = start.Day;
            var startYear = start.Year;
            var endMonth = end.Month - 1;
            var endDay = end.Day;
            var endYear = end.Year;

            WebClient client = new WebClient();

            string source = "http://chart.finance.yahoo.com/table.csv?s=" 
                                + stock 
                                + "&a=" + startMonth + "&b=" + startDay + "&c=" + startYear 
                                + "&d=" + endMonth   + "&e=" + endDay   + "&f=" + endYear
                                + "&g=d&ignore=.csv";

            Uri yahoo = new Uri(source);

            string dataStream = System.String.Empty;
            
            FlotSeries flotSeries = new FlotSeries();

            try
            {
                dataStream = client.DownloadString(yahoo);
            }
            catch (WebException)
            {
                //if not found, return an error message
                flotSeries.StockSymbol = "error";
                return Json(flotSeries, JsonRequestBehavior.AllowGet);

             }

            // First string in the array will be the header -
            // Date,Open,High,Low,Close,Volume,Adj Close
            string[] lines = dataStream.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.RemoveEmptyEntries);


            int records = lines.Length;
            string[][] tokens = new string[records][];

            for (int i = 0; i < lines.Length; i++)
            {
                tokens[i] = lines[i].Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
            }

            // Not sure if this is needed but docs say maybe.
            client.Dispose();

           
            flotSeries.StockSymbol = stock;
            flotSeries.Data = new List<decimal[]>();

            for (int j = 1; j < tokens.Length; j++)
            {
                DateTimeOffset tempDate = Convert.ToDateTime(tokens[j][0]);
                decimal closing = Convert.ToDecimal(tokens[j][4]);
                flotSeries.Data.Add(new decimal[] { tempDate.ToUnixTimeSeconds() * 1000, closing });
            }
            
            return Json(flotSeries, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Reads from file so as to lessen web requests. It's a 1.2meg file
        /// </summary>
        /// <returns>All of the stock symbols found in the file</returns>
        public JsonResult GetSymbols()
        {
            WebClient client2 = new WebClient();

            //Uncomment line below to get it directly from the website and comment the other path
            //string source = "http://oatsreportable.finra.org/OATSReportableSecurities-SOD.txt";
            string source = Server.MapPath("~/App_Data/OATSReportableSecurities-SOD.txt");
            Uri site = new Uri(source);

            string rawSymbols = System.String.Empty;
            rawSymbols = client2.DownloadString(site);

            string[] symbolLines = rawSymbols.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.RemoveEmptyEntries);
            
            //Last line is timestamp of file, no need for it and best to get rid of it here.
            int records = symbolLines.Length - 1;
            string[][] symbolsList = new string[records][];

            for (int i = 0; i < (records); i++)
            {
                symbolsList[i] = symbolLines[i].Split(new string[] { "|" }, StringSplitOptions.RemoveEmptyEntries);
            }

            SymbolList symbolList = new SymbolList();
            for (int i = 1; i < symbolsList.Length; i++)
            {
                symbolList.Symbols.Add(symbolsList[i][0]);
            }

            return Json(symbolList, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Reads from file to parse information about a specific stock
        /// </summary>
        /// <param name="sym">Stock symbol to lookup</param>
        /// <returns>Stock's Symbol, Name, and Market</returns>
        public JsonResult SymbolInfo (string sym)
        {
            string symd = sym + "|"; //ensures that it only gets the symbol so A| won't match with AA|
            string source = Server.MapPath("~/App_Data/OATSReportableSecurities-SOD.txt");
            var fileLine = System.IO.File.ReadLines(source)
                .Where(l => l.StartsWith(symd)).ToArray();

            int length = fileLine.Length;

            //if fileLine is an array > 1 then only get the first string
            string[] line = fileLine[0].Split(new string[] { "|" }, StringSplitOptions.RemoveEmptyEntries);
            SymbolInfo symbolInfo = new SymbolInfo();
            symbolInfo.Symbol = line[0];
            symbolInfo.Name = line[1];
            symbolInfo.Market = line[2];

            return Json(symbolInfo, JsonRequestBehavior.AllowGet);
        }
    }
}