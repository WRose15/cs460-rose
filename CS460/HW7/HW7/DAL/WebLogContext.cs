﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace HW7.DAL
{
    public class WebLogContext : DbContext
    {
        public WebLogContext() : base("name=WebLogContext")
        {
        }
        public DbSet<WebLog> WebLogs { get; set; }
    }
}