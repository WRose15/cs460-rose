namespace HW7
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("WebLog")]
    public partial class WebLog
    {
        public int ID { get; set; }

        public DateTime TimeStamp { get; set; }

        [Required]
        [StringLength(45)]
        public string IPAddress { get; set; }

        [Required]
        [StringLength(20)]
        public string Browser { get; set; }

        [Required]
        [StringLength(100)]
        public string Request { get; set; }
    }
}
