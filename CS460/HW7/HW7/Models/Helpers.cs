﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HW7.Models
{
    public class Helpers
    {
        /// <summary>
        /// Used to pass info to js.flot for graph
        /// </summary>
        public class FlotSeries
        {
            public string StockSymbol;
            public List<decimal[]> Data;
        }

        /// <summary>
        /// Model used to pass stock symbols
        /// </summary>
        public class SymbolList
        {
            public List<string> Symbols;
            //public List<string> names;
            //public List<string> markets;

            public SymbolList()
            {
                Symbols = new List<string>();
                //names = new List<string>();
                //markets = new List<string>();
            }
        }

        /// <summary>
        /// Model for stock info: Symbol, Name and Market. Market U is a catchall
        /// </summary>
        public class SymbolInfo
        {
            public string Symbol { get; set; }
            public string Name;
            public string Market;
        }
    }
}