﻿//Adds to drop box for symbol list with over 25,000 symbols
$(document).ready(function () {
    var source = "/JSON/FlotData/GetSymbols/";
    $.ajax({
        type: "GET",
        url: source,
        success: function (obj) {
            obj.Symbols.forEach(function (value, index) {
                var option = $('<option />').val(value).text(value);
                $("#stockSymbol").append(option);
            });
        }
    });
});

// Respond to click from the request button
$("#request").click(function () {
    var stockSym = $("#stockSymbol").val();
    var beginDay = $("#beginDay").val();
    var endDay = $("#endDay").val();

    if (beginDay === null || beginDay === "") {
        beginDay = "1/1/1900";
    }
    // use current day for end day if blank and format it for C# method
    if (endDay === null || endDay === "") {
        var jsDate = new Date();
        endDay = (jsDate.getMonth() + 1).toString() + "/"
                + jsDate.getDate().toString() + "/"
                + jsDate.getFullYear().toString();
    }

    var source = "/JSON/FlotData/GetFile/";
    $.ajax({
        type: "GET",
        data: { 'stock': stockSym, 'start': beginDay, 'end': endDay },
        dataType: "JSON",
        url: source,
        success: displayData
    });
});

// parses Json into flot format for graph. Then calls for stock info
function displayData(data) {
    $("#Values").empty();
    $("#symbolError").empty();
    if (data.StockSymbol === "error") {
        $("#symbolError").text("Stock symbol not found, please try another");
    }
    else {
        var test = [];
        test.push(data.Data);

        var options = {
            xaxis: {
                mode: "time",
                timeformat: "%m/%d/%y",
            },
            grid: {
                hoverable: true,
            },
        };

        $.plot($("#placeholder"), test, options);
        var source2 = "/JSON/FlotData/SymbolInfo/"
        $.ajax({
            type: "GET",
            data: { 'sym': data.StockSymbol },
            dataType: "JSON",
            url: source2,
            success: postInfo
        });

        $("#placeholder").bind("plothover", function (event, pos, item) {
            if (item) {
                var str = "Date: " + formatDate(pos.x.toFixed(0)) + ", Price: $" + pos.y.toFixed(2) + "";
                $("#Values").text(str);
            }
        });

        function formatDate(date) {
            var test = Math.round(date);
            var d = new Date(test);

            month = (d.getMonth() + 1);
            day = '' + d.getDate();
            year = '' + d.getFullYear();
            
            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;

            return [month, day, year].join('/');
        }
    }
}

//places stock symbol, name and market into view
function postInfo(symInfo) {
    $("#stockLabel").text("Stock:");
    $("#stock").text(symInfo.Symbol);
    $("#nameLabel").text("Name:");
    $("#name").text(symInfo.Name);
    $("#marketLabel").text("Market:");
    $("#market").text(symInfo.Market);
}

