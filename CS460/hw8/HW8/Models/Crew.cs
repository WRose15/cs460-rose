namespace HW8.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Crew")]
    public partial class Crew
    {
        public int CrewID { get; set; }

        public int PirateID { get; set; }

        public int ShipID { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString ="{0:C}")]
        [Display(Name = "Booty Value")]
        public decimal BootyValue { get; set; }

        [Display(Name = "Pirate Name")]
        public virtual Pirate Pirate { get; set; }

        [Display(Name = "Ship Name")]
        public virtual Ship Ship { get; set; }
    }
}
