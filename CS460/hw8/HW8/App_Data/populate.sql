﻿-- ########### Pirates #########
INSERT INTO [dbo].[Pirates](PirateName, ShanghaiDate)VALUES('Toga', '1975-01-27');
INSERT INTO [dbo].[Pirates](PirateName, ShanghaiDate)VALUES('Squeaky McRatatattat', '1982-10-5');
INSERT INTO [dbo].[Pirates](PirateName, ShanghaiDate)VALUES('Mortimer Morter', '1999-12-17');
INSERT INTO [dbo].[Pirates](PirateName, ShanghaiDate)VALUES('Slimfast McGrueder', '2002-03-9');
INSERT INTO [dbo].[Pirates](PirateName, ShanghaiDate)VALUES('Topknot Harry', '2005-03-21');

-- ########### Ships ###########
INSERT INTO [dbo].[Ships](ShipName, ShipType, Tonnage)VALUES('Mary Celeste', 'Brigantine', 198);
INSERT INTO [dbo].[Ships](ShipName, ShipType, Tonnage)VALUES('Black Hawk', 'Spanish Galleon', 1200);
INSERT INTO [dbo].[Ships](ShipName, ShipType, Tonnage)VALUES('Crossed Bones', 'Frigate', 298);
INSERT INTO [dbo].[Ships](ShipName, ShipType, Tonnage)VALUES('Lightning Strike', 'Frigate', 318);
INSERT INTO [dbo].[Ships](ShipName, ShipType, Tonnage)VALUES('Nossa Senhora do Cabo', 'British Man-o-War', 3500);

-- ########### Crew ############
INSERT INTO [dbo].[Crew](PirateID, ShipID, BootyValue)VALUES( 1, 1, 10);
INSERT INTO [dbo].[Crew](PirateID, ShipID, BootyValue)VALUES( 2, 1, 15);
INSERT INTO [dbo].[Crew](PirateID, ShipID, BootyValue)VALUES( 3, 1, 12);
INSERT INTO [dbo].[Crew](PirateID, ShipID, BootyValue)VALUES( 4, 1, 16);
INSERT INTO [dbo].[Crew](PirateID, ShipID, BootyValue)VALUES( 5, 1, 12);
INSERT INTO [dbo].[Crew](PirateID, ShipID, BootyValue)VALUES( 1, 2, 232);
INSERT INTO [dbo].[Crew](PirateID, ShipID, BootyValue)VALUES( 2, 2, 200);
INSERT INTO [dbo].[Crew](PirateID, ShipID, BootyValue)VALUES( 3, 2, 323);
INSERT INTO [dbo].[Crew](PirateID, ShipID, BootyValue)VALUES( 4, 2, 531);
INSERT INTO [dbo].[Crew](PirateID, ShipID, BootyValue)VALUES( 5, 2, 198);
INSERT INTO [dbo].[Crew](PirateID, ShipID, BootyValue)VALUES( 1, 3, 50);
INSERT INTO [dbo].[Crew](PirateID, ShipID, BootyValue)VALUES( 2, 3, 58);
INSERT INTO [dbo].[Crew](PirateID, ShipID, BootyValue)VALUES( 3, 3, 81);
INSERT INTO [dbo].[Crew](PirateID, ShipID, BootyValue)VALUES( 4, 4, 75);
INSERT INTO [dbo].[Crew](PirateID, ShipID, BootyValue)VALUES( 5, 4, 68);
INSERT INTO [dbo].[Crew](PirateID, ShipID, BootyValue)VALUES( 1, 4, 93);
INSERT INTO [dbo].[Crew](PirateID, ShipID, BootyValue)VALUES( 2, 5, 842);
INSERT INTO [dbo].[Crew](PirateID, ShipID, BootyValue)VALUES( 3, 5, 618);
INSERT INTO [dbo].[Crew](PirateID, ShipID, BootyValue)VALUES( 4, 5, 728);
INSERT INTO [dbo].[Crew](PirateID, ShipID, BootyValue)VALUES( 5, 5, 918);
