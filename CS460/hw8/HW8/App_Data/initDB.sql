﻿-- Creates database
-- ########### Attach ############
CREATE DATABASE [PiratesDB] ON
PRIMARY (NAME=[PiratesDB], FILENAME='$(dbdir)\PiratesDB.mdf')
LOG ON (NAME=[PiratesDB_log], FILENAME='$(dbdir)\PiratesDB_log.ldf');
GO
USE [PiratesDB];
-- ########### Pirates ###########
CREATE TABLE [dbo].[Pirates]
(
	[PirateID] INT IDENTITY (1,1) NOT NULL,
	[PirateName] NVARCHAR (50) NOT NULL,
	[ShanghaiDate] DATE NOT NULL,
	CONSTRAINT [PK_dbo.Pirates] PRIMARY KEY CLUSTERED ([PirateID] ASC)
);

-- ########### Ships ###########
CREATE TABLE [dbo].[Ships]
(
	[ShipID] INT IDENTITY (1,1) NOT NULL,
	[ShipName] NVARCHAR (100) NOT NULL,
	[ShipType] NVARCHAR (100) NOT NULL,
	[Tonnage] INT,
	CONSTRAINT [PK_dbo.Ships] PRIMARY KEY CLUSTERED ([ShipID] ASC),
--	CONSTRAINT [FK_dbo.Groups_dbo.Advisors_ID] FOREIGN KEY ([AdvisorID]) 
--		REFERENCES [dbo].[Advisors] ([ID])
);

-- ########### Crew ###########
CREATE TABLE [dbo].[Crew]
(
	[CrewID] INT IDENTITY (1,1) NOT NULL,
	[PirateID] INT NOT NULL,
	[ShipID] INT NOT NULL,
	[BootyValue] DECIMAL NOT NULL,
	CONSTRAINT [PK_dbo.CrewID] PRIMARY KEY CLUSTERED ([CrewID] ASC),
	CONSTRAINT [FK_dbo.Crew_dbo.Pirates_PirateID] FOREIGN KEY ([PirateID]) REFERENCES [dbo].[Pirates] ([PirateID]),
	CONSTRAINT [FK_dbo.Crew_dbo.Ships_ShipID] FOREIGN KEY ([ShipID]) REFERENCES [dbo].[Ships] ([ShipID])
);

USE master;
GO

ALTER DATABASE [PiratesDB] SET SINGLE_USER WITH ROLLBACK IMMEDIATE
GO

EXEC sp_detach_db 'PiratesDB', 'true'