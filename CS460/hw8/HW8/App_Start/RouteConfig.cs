﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace HW8
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Ships",
                url: "Ships/{action}/{id}",
                defaults: new { controller = "Ship", action = "Ships", id = UrlParameter.Optional }
                );

            routes.MapRoute(
                name: "Pirates",
                url: "Pirates/{action}/{id}",
                defaults: new { controller = "Pirate", action = "Pirates", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Crews",
                url: "Crews/{action}/{id}",
                defaults: new { controller = "Crew", action = "Crews", id = UrlParameter.Optional }
                );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
