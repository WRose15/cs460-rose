﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HW6.ViewModels
{
    /// <summary>
    /// Allows data from the table Production.Category to be passed into the
    /// menu so that the menu view can read the data types. This allows
    /// any page that uses the menu to have a different model set.
    /// </summary>
    public class MenuCategoryViewModel
    {
        public int CategoryID { get; set; }
        public string Name { get; set; }
    }

    /// <summary>
    /// Allows data from the table Production.Subcategory to be passed into
    /// the menu so that the menu view can read the data types. This allows
    /// any page that uses the menu to have a different model set.
    /// </summary>
    public class MenuSubcategoryViewModel
    {
        public int SubcategoryID { get; set; }
        public int CategoryID { get; set; }
        public string SubName { get; set; }
    }
}