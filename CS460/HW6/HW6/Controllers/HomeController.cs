﻿using HW6.Models;
using HW6.ViewModels;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace HW6.Controllers
{


    public class HomeController : Controller
    {
        private AWContext db = new AWContext();
        public int PageSize = 7;

        /// <summary>
        /// Renders the top menu bar for all of the site's webpages. It is 
        /// not strongly typed so the View Model will not conflict with the
        /// model of the page that losts this partial view
        /// </summary>
        /// <returns>
        /// ViewBag.CategoryNames - returns a list of CategoryID, Name pairs.
        /// ViewBag.SubcategoryNames - returns a list of ProductCategoryID, 
        /// and ProductSubcategoryID
        /// </returns>
        public PartialViewResult Menu()
        {
            ViewBag.CategoryNames = db.ProductCategories.Select(s => new MenuCategoryViewModel { CategoryID = s.ProductCategoryID, Name = s.Name }).ToList();
            ViewBag.SubcategoryNames = db.ProductSubcategories.Select(sc => new MenuSubcategoryViewModel { CategoryID = sc.ProductCategoryID, SubcategoryID = sc.ProductSubcategoryID, SubName = sc.Name }).ToList();
            return PartialView();
        }

        /// <summary>
        /// Front landing page for site.
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Shows all products of a subcategory in a table.
        /// </summary>
        /// <param name="id">Subcategory id</param>
        /// <returns>
        /// Product Model
        /// </returns>
        public ActionResult SubCategoryList(int? id=1, int page = 1)
        {
            Debug.WriteLine(page);
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var products = db.Products.Where(p => p.ProductSubcategoryID == id)
                            .OrderBy(p => p.ProductID)
                            .Skip((page - 1) * PageSize)
                            .Take(PageSize);
            ViewBag.CatName = db.ProductSubcategories.Find(id).Name;
            ViewBag.Page = page;
            ViewBag.PageCount = (db.Products.Where(p => p.ProductSubcategoryID == id).Count()) / PageSize;

            return View(products);
        }

        /// <summary>
        /// Shows details of a single item including reviews.
        /// </summary>
        /// <param name="id">ProductID of item</param>
        /// <returns>A single product record</returns>
        public ActionResult ItemPage(int? id= 771)
        {
           if (id <= 0 )
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            try
            {
                var product = db.Products.Where(p => p.ProductID == id);
                return View(product);
            }
            // In case the product doesn't exist, use a known product. Ideally
            // this should return an error really.
            catch (NullReferenceException)
            {
                var product = db.Products.Where(p => p.ProductID == 771);
                return View(product);
            }
        }

        /// <summary>
        /// HttpGet method
        /// Creates new ProductReview and sets some fields of the model so a
        /// a customer may submit a review of the item. 
        /// </summary>
        /// <param name="id">ProductID of item to review</param>
        /// <returns>A ProductReview object with some fields preset</returns>
        [HttpGet]
        public ActionResult ItemReview(int? id = 709)
        {
            ProductReview review = new ProductReview();
            review.ProductID = (int)id;
            review.ReviewDate = DateTime.Now;
            review.ModifiedDate = DateTime.Today;
            review.Rating = 5;
            try
            {
                ViewBag.ProductName = db.Products.Where(p => p.ProductID == (int)id).FirstOrDefault().ProductModel.Name;
            } catch (NullReferenceException e)
            {
                Console.WriteLine(e.StackTrace);
                ViewBag.ProductName = "Not Available";
            }
            return View(review);
        }

        /// <summary>
        /// HttpPost method for item review. If model form is valid
        /// it will enter it into the database and redirect to the 
        /// home page. Otherwise, it'll return to the view with 
        /// the errors marked.
        /// </summary>
        /// <param name="review">The form for ProductReview model</param>
        /// <returns>Two different views depending on success</returns>
        [HttpPost]
        public ActionResult ItemReview(ProductReview review)
        {
            if (ModelState.IsValid)
            {
                db.ProductReviews.Add(review);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(review);
        }
    }
}