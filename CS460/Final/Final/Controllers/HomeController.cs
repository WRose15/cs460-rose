﻿using Final.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Final.Controllers
{
    public class HomeController : Controller
    {
        public ArtContext db = new ArtContext();

        public ActionResult Index()
        {
            var genres = db.Genres.ToList();
            return View(genres);
        }

        public ActionResult Genres()
        {
            var genres = db.Genres.ToList();

            return View(genres);
        }

        public ActionResult ArtWorks()
        {
            var artworks = db.ArtWorks.ToList();
            return View(artworks);
        }

        public ActionResult Artists()
        {
            var artists = db.Artists.ToList();
            return View(artists);
        }

        public ActionResult ArtistEdit (int? ID)
        {
            var artist = db.Artists.Find(ID);

            return View(artist);
        }

        public ActionResult ArtistDetails(int? ID)
        {
            var artist = db.Artists.Find(ID);

            return View(artist);
        }
        
        public ActionResult ArtistDelete(int? ID)
        {
            var artist = db.Artists.Find(ID);

            return View(artist);
        }
           
        [HttpGet]
        public ActionResult ArtistCreate()
        {
            Artist person = new Artist();

            return View(person);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ArtistCreate(Artist info)
        {
            if(ModelState.IsValid)
            {
                db.Entry(info).State = System.Data.Entity.EntityState.Added;
                db.SaveChanges();
                return RedirectToAction("Artists");
            }
            return View(info);
        }

        public JsonResult GetGenres()
        {
            var genres = db.Genres.ToList();

            return Json(genres, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DisplayArt(string id)
        {
            var names = db.Classifications.Where(x => x.ArtWork == id).ToList();
                          
            return Json(id, JsonRequestBehavior.AllowGet);
        }
    }
}