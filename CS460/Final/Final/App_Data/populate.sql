﻿-- ########### Artists #########
INSERT INTO [dbo].[Artists](ArtistName, BirthDate, BirthCity)VALUES('M.C. Escher', '1898-6-17', 'Leeuwarden, Netherlands');
INSERT INTO [dbo].[Artists](ArtistName, BirthDate, BirthCity)VALUES('Leonardo Da Vinci', '1519-5-2', 'Vinci, Italy');
INSERT INTO [dbo].[Artists](ArtistName, BirthDate, BirthCity)VALUES('Hatip Mehmed Efendi', '1680-11-18', 'Unknown');
INSERT INTO [dbo].[Artists](ArtistName, BirthDate, BirthCity)VALUES('Salvador Dali', '1904-5-11', 'Figueres, Spain');

-- ########### ArtWorks ###########
INSERT INTO [dbo].[ArtWorks](Title, Artist)VALUES('Circle Limit III', 'M.C. Escher');
INSERT INTO [dbo].[ArtWorks](Title, Artist)VALUES('Twon Tree', 'M.C. Escher');
INSERT INTO [dbo].[ArtWorks](Title, Artist)VALUES('Mona Lisa', 'Leonardo Da Vinci');
INSERT INTO [dbo].[ArtWorks](Title, Artist)VALUES('The Vitruvian Man', 'Leonardo Da Vinci');
INSERT INTO [dbo].[ArtWorks](Title, Artist)VALUES('Ebru', 'Hatip Mehmed Efendi');
INSERT INTO [dbo].[ArtWorks](Title, Artist)VALUES('Honey Is Sweeter Than Blood', 'Salvadro Dali');

-- ########### Genres ##############
INSERT INTO [dbo].[Genres](Name)VALUES('Tesselation');
INSERT INTO [dbo].[Genres](Name)VALUES('Surrealism');
INSERT INTO [dbo].[Genres](Name)VALUES('Portrait');
INSERT INTO [dbo].[Genres](Name)VALUES('Renaissance');

-- ########### Classifications #####
INSERT INTO [dbo].[Classifications](ArtWork, Genre)VALUES('Circle Limit III', 'Tesselation');
INSERT INTO [dbo].[Classifications](ArtWork, Genre)VALUES('Twon Tree', 'Tesselation');
INSERT INTO [dbo].[Classifications](ArtWork, Genre)VALUES('Twon Tree', 'Surrealism');
INSERT INTO [dbo].[Classifications](ArtWork, Genre)VALUES('Mona Lisa', 'Portrait');
INSERT INTO [dbo].[Classifications](ArtWork, Genre)VALUES('Mona Lisa', 'Renaissance');
INSERT INTO [dbo].[Classifications](ArtWork, Genre)VALUES('The Vitruvian Man', 'Renaissance');
INSERT INTO [dbo].[Classifications](ArtWork, Genre)VALUES('Ebru', 'Tesselation');
INSERT INTO [dbo].[Classifications](ArtWork, Genre)VALUES('Circle Limit III', 'Tesselation');
INSERT INTO [dbo].[Classifications](ArtWork, Genre)VALUES('Honey Is Sweeter Than Blood', 'Surrealism');

