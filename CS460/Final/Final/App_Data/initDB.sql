﻿
-- ########### Artists ###########
CREATE TABLE [dbo].[Artists]
(
	[ID] INT IDENTITY (1,1) NOT NULL,
	[ArtistName] NVARCHAR (50) NOT NULL,
	[BirthDate] DATE NOT NULL,
	[BirthCity] NVARCHAR (50) NOT NULL,
	CONSTRAINT [PK_dbo.Artists] PRIMARY KEY CLUSTERED ([ID] ASC)
);

-- ########### ArtWorks ###########
CREATE TABLE [dbo].[ArtWorks]
(
	[ID] INT IDENTITY (1,1) NOT NULL,
	[Title] NVARCHAR (100) UNIQUE NOT NULL,
	[Artist] NVARCHAR (100) NOT NULL,
	CONSTRAINT [PK_dbo.ArtWorks] PRIMARY KEY CLUSTERED ([ID] ASC),

);

-- ########### Genres ###########
CREATE TABLE [dbo].[Genres]
(
	[ID] INT IDENTITY (1,1) NOT NULL,
	[Name] NVARCHAR (20) NOT NULL,
	CONSTRAINT [PK_dbo.GenreID] PRIMARY KEY CLUSTERED ([ID] ASC),
);

-- ########### Classifications ###
CREATE TABLE [dbo].[Classifications]
(
	[ClassID] INT IDENTITY (1,1) NOT NULL,
	[ArtWork] NVARCHAR (100) NOT NULL,
	[Genre] NVARCHAR (100) NOT NULL,
	CONSTRAINT [PK_dbo.GenreID] PRIMARY KEY CLUSTERED ([ClassID] ASC),
	CONSTRAINT [FK_dbo.Classifications_ArtWork_dbo.ArtWorks_Title] FOREIGN KEY ([ArtWork]) REFERENCES [dbo].[ArtWorks] ([Title])
);
